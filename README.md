# Anomaly-detection-using-a-meta-heuristic-approach
A theory mini project as a part of course CO251[Software engineering] 

Anomaly detection is the problem of identifying data points that don't conform to expected (normal) behaviour. Unexpected data points are also known as outliers and exceptions etc. Anomaly detection has crucial significance in the wide variety of domains as it provides critical and actionable information
